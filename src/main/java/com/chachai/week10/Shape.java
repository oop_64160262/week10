package com.chachai.week10;

public abstract class Shape {
    private String name;
    public Shape(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public abstract double calArea(); // สิ่งที่ไม่สามารถคำนวณได้ใน method นี้
    public abstract double calPerimeter();
}